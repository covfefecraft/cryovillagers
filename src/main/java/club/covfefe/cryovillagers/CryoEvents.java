package club.covfefe.cryovillagers;

import org.bukkit.entity.Villager;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.Collection;

// Events
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.ChunkLoadEvent;

public final class CryoEvents implements Listener
{
  private Cryo plugin;

  public 
  CryoEvents(Cryo plugin)
  {
    this.plugin = plugin;
  }

  // Unfreeze villagers on first interaction
  @EventHandler
  public void 
  onEntityInteract(PlayerInteractEntityEvent event)
  {
    if(event.getRightClicked() instanceof Villager)
    {
      Villager v = (Villager)(event.getRightClicked());
      v.setMetadata(Cryo.INTERACTED_TAGNAME, new FixedMetadataValue(plugin, true));
      plugin.setAiFor(v, true);
    }
  }

  // Unfreeze villagers on chunk unload
  @EventHandler
  public void
  onChunkUnload(ChunkUnloadEvent event)
  {
    Collection<Villager> vs = getVillagersIn(event.getChunk().getEntities());

    for (Villager v : vs)
      plugin.setAiFor(v, true);
  }

  // Freeze villagers on chunk load
  @EventHandler
  public void
  onChunkLoad(ChunkLoadEvent event)
  {
    Collection<Villager> vs = getVillagersIn(event.getChunk().getEntities());

    for (Villager v : vs)
      plugin.setAiFor(v, !plugin.shouldSleep(v));
  }

  // Helper to get all villagers from a chunk
  private static ArrayList<Villager>
  getVillagersIn(Entity[] es)
  {
    ArrayList<Villager> vs = new ArrayList<Villager>();

    for(Entity e : es)
      if (e instanceof Villager)
        vs.add((Villager)e);

    return vs;
  }
}
