package club.covfefe.cryovillagers;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Villager;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Collection;

public final class Cryo extends JavaPlugin
{
  public static final String INTERACTED_TAGNAME = "interactedWith";
  public static final String FROZEN_TAGNAME = "isFrozen";

  private CryoConfig config;

  // Set a villager's AI
  public void
  setAiFor(Villager v, Boolean to)
  {
    if(CryoUtil.getMetadataValueFor(v, FROZEN_TAGNAME, this)
        .orElse(new FixedMetadataValue(this, false))
        .asBoolean() == !to) 
      return;

    if(v.getEntitySpawnReason() != SpawnReason.CUSTOM) 
    {
      v.setAI(true); // Remove depreciated tag 

      if (config.isDebugMode) 
      {
        // Debug glowing effect
        CryoDebug.debugGlowingEffect(v, this, to);

        // Debug logging
        Location l = v.getLocation();
        String log = 
          new StringBuilder("Setting the AI of the villager")
          .append(" at (").append(l.getBlockX()).append(',').append(l.getBlockZ()).append(')')
          .append(" to ").append(to).append('.')
          .toString();

        CryoDebug.debugLog(log, true);
      }

      // Set the AI of this villager
      v.setAware(to);
      v.setAgeLock(!to);
      v.setCollidable(to);
      v.setInvulnerable(!to);
      v.setSilent(!to && config.muteFrozenVillagers);

      // Mark this villager as frozen or not frozen
      v.setMetadata(FROZEN_TAGNAME, new FixedMetadataValue(this, !to));

      // Replenish the trades of this villager when awakened
      v.getRecipes().forEach(it -> it.setUses(0));
    }
  }

  // Check if a villager should sleep
  public boolean
  shouldSleep(Villager v)
  {
    return 
      !(CryoUtil.getMetadataValueFor(v, INTERACTED_TAGNAME, this).isPresent()
        || (v.getCustomName() != null 
          && config
             .insomniacTags
             .stream()
             .anyMatch(it -> it.equalsIgnoreCase(v.getCustomName()))))
      ;
  }

  @Override
  public void 
  onEnable()
  {
    BukkitScheduler scheduler = getServer().getScheduler();
    config = new CryoConfig(this);
    CryoDebug.debugLog("Running with debug logging", config.isDebugMode);

    // Register the task to give newly loaded villagers noai and force existing ones to sleep if it's night
    scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
      public void 
      run() 
      {
        for(World w : config.affectedWorlds)
        {
          Collection<Villager> villagers = w.getEntitiesByClass(Villager.class);

          for(Villager v : villagers)
          {
            boolean shouldSleep = shouldSleep(v);
            setAiFor(v, !shouldSleep);
          }
        }
      } 
    }, 0L, config.freezeInterval);

    // Register an event to awaken villagers
    getServer().getPluginManager().registerEvents(new CryoEvents(this), this);
  }

  // Re-enable all villager AIs on server reload
  @Override
  public void
  onDisable()
  { 
    for(World w : config.affectedWorlds)
    {
      Collection<Villager> villagers = w.getEntitiesByClass(Villager.class);

      for(Villager v : villagers)
        setAiFor(v, true);
    }
  }
}
