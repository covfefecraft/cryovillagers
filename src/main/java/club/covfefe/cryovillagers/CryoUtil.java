package club.covfefe.cryovillagers;

import java.util.Optional;

import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.plugin.java.JavaPlugin;

public final class CryoUtil
{
  // Make this class a Singleton
  private CryoUtil() { }

  // Helper to get this plugin's metadatavalue out of the set
  public static final Optional<MetadataValue>
  getMetadataValueFor(Metadatable m, String tag, JavaPlugin p)
  {
    return
      m.getMetadata(tag)
       .stream()
       .filter(it -> it.getOwningPlugin() == p)
       .findFirst()
      ;
  }
}
