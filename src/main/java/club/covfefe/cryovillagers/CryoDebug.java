package club.covfefe.cryovillagers;

import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.entity.Villager;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

public final class CryoDebug
{
    public static final String GLOWINGFROMDBG_TAGNAME = "glowingFromDebug";
    protected CryoDebug() {}

    // Debug glowing effect
    public static void
    debugGlowingEffect(Villager v, JavaPlugin p, boolean to)
    {
      if (!to) 
        v.setMetadata(GLOWINGFROMDBG_TAGNAME, new FixedMetadataValue(p, !v.isGlowing()));

      Optional<MetadataValue> hasGlowingAttr = CryoUtil.getMetadataValueFor(v, GLOWINGFROMDBG_TAGNAME, p);

      if (hasGlowingAttr.isEmpty() || hasGlowingAttr.get().asBoolean())
        v.setGlowing(!to);
    }

    // Debug logging
    public static void
    debugLog(String msg, boolean debugMode)
    {
      if (debugMode)
        Bukkit.getLogger().info("CRYO::DEBUG > " + msg);
    }
}
