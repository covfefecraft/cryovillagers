package club.covfefe.cryovillagers;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class CryoConfig
{
  // Worlds affected by villager freezing
  public final List<World> affectedWorlds;
  private static final String AFFECTED_WORLDS_OPTPATH = "affectedWorlds"; 

  // Which tags prevent villager freezing
  public final List<String> insomniacTags;
  private static final String INSOMNIAC_OPTPATH = "insomniacTags";

  // Use debugging systems
  public final boolean isDebugMode;
  private static final String DEBUG_OPTPATH = "isDebugMode";

  // How often to check for new villagers to freeze
  public final long freezeInterval;
  private static final String FREEZE_INTERVAL_OPTPATH = "freezeInterval"; 

  // Mute frozen villagers
  public final boolean muteFrozenVillagers;
  private static final String MUTE_VILLAGERS_OPTPATH = "muteFrozenVillagers"; 
  
  // Create a representation of the plugin's config file
  public
  CryoConfig(JavaPlugin p)
  {
    FileConfiguration c = prepConfig(p);

    affectedWorlds = getAffectedWorlds(p, c.getStringList(AFFECTED_WORLDS_OPTPATH));

    insomniacTags = c.getStringList(INSOMNIAC_OPTPATH);
    isDebugMode = c.getBoolean(DEBUG_OPTPATH);
    freezeInterval = c.getLong(FREEZE_INTERVAL_OPTPATH);
    muteFrozenVillagers = c.getBoolean(MUTE_VILLAGERS_OPTPATH);
  }

  // Prepare the configuration file for use
  private static FileConfiguration
  prepConfig(JavaPlugin p)
  {
    p.saveDefaultConfig();

    FileConfiguration c = p.getConfig();
    c.options().copyHeader(true);
    c.options().copyDefaults(true);
    p.saveConfig();

    return c; 
  }

  // Setup the affected worlds option
  private static List<World>
  getAffectedWorlds(JavaPlugin p, List<String> names)
  {
    return names.stream()
      .map(it -> p.getServer().getWorld(it))
      .filter(it -> it != null)
      .collect(Collectors.toList());
  }
}
