build: clean
	mvn package	
	cp target/*.jar testsrv/plugins/

run: build
	testsrv/run.sh

clean:
	rm -rf target/cryo* testsrv/plugins/cryo*
