### CryoVillagers

##### Introduction
CryoVillagers is a PaperMC plugin which cryogenically freezes villagers to reduce their impact on the world. When frozen, villagers are much more performant than usual, as they do not try to use their expensive AIs. To preserve natural villages, villagers who are frozen are invincible as well.
