# CryoVillagers TODO
### 1.0 Milestone
- DONE Debugging options to allow for easier testing
- DONE Configuration options such as how often to freeze new villagers
- DONE Fine-tune the freezing system to freeze as much of the villager as possible without clogging farms
- DONE Allow freezing villagers on non-overworld world ids
- TODO Fix any bugs that crop up

### Possible Future
- TODO Freeze villagers who have not been interacted with in a while

### Misc
- DONE Move the build system to a Makefile
